package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;
import org.apache.jackrabbit.webdav.version.DeltaVConstants;
import org.apache.jackrabbit.webdav.version.report.Report;
import org.apache.jackrabbit.webdav.version.report.ReportInfo;
import org.apache.jackrabbit.webdav.version.report.ReportType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AddressbookMultigetReport implements Report, DeltaVConstants {
    public static final ReportType TYPE = ReportType.register("addressbook-multiget", CardDavConstants.CARDDAV_NAMESPACE, AddressbookMultigetReport.class);
    private ReportInfo info;
    private DavResource resource;

    @Override
    public ReportType getType() {
        return TYPE;
    }

    @Override
    public boolean isMultiStatusReport() {
        return true;
    }

    @Override
    public void init(DavResource resource, ReportInfo info) throws DavException {
        setResource(resource);
        setInfo(info);
    }

    private void setResource(DavResource resource) throws DavException {
        this.resource = resource;
    }

    private void setInfo(ReportInfo info) throws DavException {
        if (!getType().isRequestedReportType(info)) {
            throw new DavException(DavServletResponse.SC_BAD_REQUEST, "DAV:version-tree element expected.");
        }
        this.info = info;
    }

    @Override
    public Element toXml(Document document) {
        return getMultiStatus().toXml(document);
    }

    private MultiStatus getMultiStatus() {
        if (info == null || resource == null) {
            throw new NullPointerException("Error while running DAV:version-tree report");
        }

        MultiStatus ms = new MultiStatus();
        buildResponse(resource, info.getPropertyNameSet(), ms);
        return ms;
    }
    private void buildResponse(DavResource res, DavPropertyNameSet propNameSet, MultiStatus ms) {
        DavResourceIterator it = res.getMembers();
        while (it.hasNext()) {
            DavResource childRes = it.nextResource();
            if (childRes instanceof VCardResource) {
                VCardResource vcard = (VCardResource)childRes;
                if (propNameSet.isEmpty()) {
                    ms.addResourceStatus(vcard, DavServletResponse.SC_OK, 0);
                } else {
                    ms.addResourceProperties(vcard, propNameSet, 0);
                }
            }
        }
    }
}