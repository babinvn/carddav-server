package com.mesilat.carddav.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavMethods;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.FileLogger;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.WebdavRequest;
import org.apache.jackrabbit.webdav.WebdavRequestImpl;
import org.apache.jackrabbit.webdav.WebdavResponse;
import org.apache.jackrabbit.webdav.WebdavResponseImpl;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;
import org.apache.jackrabbit.webdav.util.CSRFUtil;
import org.apache.jackrabbit.webdav.version.report.Report;
import org.apache.jackrabbit.webdav.version.report.ReportInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

public class CardDavServlet extends HttpServlet implements DavConstants {
    private static final Logger LOGGER = LoggerFactory.getLogger(Constants.LOG_NAME);

    public static final String INIT_PARAM_RESOURCE_PATH_PREFIX = "resource-path-prefix";
    public static final String CTX_ATTR_RESOURCE_PATH_PREFIX = "jackrabbit.webdav.simple.resourcepath";
    public static final String INIT_PARAM_CSRF_PROTECTION = "csrf-protection";
    public static final String INIT_PARAM_CREATE_ABSOLUTE_URI = "createAbsoluteURI";
    public static final String INIT_PARAM_LOGGING_DIR = "loggingDir";

    private CSRFUtil csrfUtil;
    private boolean createAbsoluteURI = true;
    private String resourcePathPrefix;
    private DavLocatorFactory locatorFactory;
    private DavResourceFactory resourceFactory;
    private String loggingDir;

    @Override
    public void init() throws ServletException {
        super.init();

        resourcePathPrefix = getInitParameter(INIT_PARAM_RESOURCE_PATH_PREFIX);
        if (resourcePathPrefix == null) {
            LOGGER.debug("Missing path prefix > setting to empty string.");
            resourcePathPrefix = "";
        } else if (resourcePathPrefix.endsWith("/")) {
            LOGGER.debug("Path prefix ends with '/' > removing trailing slash.");
            resourcePathPrefix = resourcePathPrefix.substring(0, resourcePathPrefix.length() - 1);
        }
        getServletContext().setAttribute(CTX_ATTR_RESOURCE_PATH_PREFIX, resourcePathPrefix);
        LOGGER.info(INIT_PARAM_RESOURCE_PATH_PREFIX + " = '" + resourcePathPrefix + "'");


        // read csrf protection params
        String csrfParam = getInitParameter(INIT_PARAM_CSRF_PROTECTION);
        csrfUtil = new CSRFUtil(csrfParam);
        LOGGER.debug(INIT_PARAM_CSRF_PROTECTION + " = " + csrfParam);

        //create absolute URI hrefs..
        String param = getInitParameter(INIT_PARAM_CREATE_ABSOLUTE_URI);
        if (param != null) {
            createAbsoluteURI = Boolean.parseBoolean(param);
        }
        LOGGER.debug(INIT_PARAM_CREATE_ABSOLUTE_URI + " = " + createAbsoluteURI);

        loggingDir = getInitParameter(INIT_PARAM_LOGGING_DIR);
    }
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRemoteUser() == null){
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        WebdavRequest webdavRequest = new WebdavRequestImpl(request, getLocatorFactory(), isCreateAbsoluteURI(), new TheFileLogger(new File(String.format("%s/%s", loggingDir, request.getRemoteUser()))));

        // DeltaV requires 'Cache-Control' header for all methods except 'VERSION-CONTROL' and 'REPORT'.
        int methodCode = DavMethods.getMethodCode(request.getMethod());
        boolean noCache = DavMethods.isDeltaVMethod(webdavRequest) && !(DavMethods.DAV_VERSION_CONTROL == methodCode || DavMethods.DAV_REPORT == methodCode);
        WebdavResponse webdavResponse = new WebdavResponseImpl(response, noCache, new TheFileLogger(new File(String.format("%s/%s", loggingDir, request.getRemoteUser()))));

        try {
            
            // perform referrer host checks if CSRF protection is enabled
            if (!csrfUtil.isValidRequest(webdavRequest)) {
                webdavResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            
            DavResource resource = getResourceFactory().createResource(webdavRequest.getRequestLocator(), webdavRequest, webdavResponse);
            if (!isPreconditionValid(webdavRequest, resource)) {
                webdavResponse.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
                return;
            }
            if (!execute(webdavRequest, webdavResponse, methodCode, resource)) {
                super.service(request, response);
            }
        } catch (DavException ex) {
            webdavResponse.sendError(ex);
        }
    }
    protected boolean isCreateAbsoluteURI() {
        return createAbsoluteURI;
    }
    public DavLocatorFactory getLocatorFactory() {
        if (locatorFactory == null) {
            locatorFactory = new LocatorFactoryImplEx(resourcePathPrefix);
        }
        return locatorFactory;
    }
    public DavResourceFactory getResourceFactory() {
        if (resourceFactory == null) {
            resourceFactory = new ResourceFactoryImpl(DatabaseService.getInstance().getUserKey(), DatabaseService.getInstance().getUserDisplayName());
        }
        return resourceFactory;
    }
    protected boolean isPreconditionValid(WebdavRequest request, DavResource resource) {
        return !resource.exists() || request.matchesIfHeader(resource);
    }
    protected boolean execute(WebdavRequest request, WebdavResponse response,
                              int method, DavResource resource)
            throws ServletException, IOException, DavException {

        switch (method) {
            case DavMethods.DAV_PROPFIND:
                doPropFind(request, response, resource);
                break;
            case DavMethods.DAV_OPTIONS:
                doOptions(request, response, resource);
                break;
            case DavMethods.DAV_REPORT:
                doReport(request, response, resource);
                break;
            default:
                // any other method
                return false;
        }
        return true;
    }
    protected void doPropFind(WebdavRequest request, WebdavResponse response, DavResource resource) throws IOException, DavException {
        if (!resource.exists()) {
            response.sendError(DavServletResponse.SC_NOT_FOUND);
            return;
        }

        int depth = request.getDepth(DEPTH_INFINITY);
        DavPropertyNameSet requestProperties = request.getPropFindProperties();
        int propfindType = request.getPropFindType();

        MultiStatus mstatus = new MultiStatus();
        mstatus.addResourceProperties(resource, requestProperties, propfindType, depth);
        response.sendMultiStatus(mstatus);
    }
    protected void doOptions(WebdavRequest request, WebdavResponse response, DavResource resource) throws IOException, DavException {
        response.addHeader(DavConstants.HEADER_DAV, resource.getComplianceClass());
        response.addHeader("Allow", resource.getSupportedMethods());
        response.addHeader("MS-Author-Via", DavConstants.HEADER_DAV);
/*
        if (resource instanceof SearchResource) {
            String[] langs = ((SearchResource) resource).getQueryGrammerSet().getQueryLanguages();
            for (String lang : langs) {
                response.addHeader(SearchConstants.HEADER_DASL, "<" + lang + ">");
            }
        }
        // with DeltaV the OPTIONS request may contain a Xml body.
        OptionsResponse oR = null;
        OptionsInfo oInfo = request.getOptionsInfo();
        if (oInfo != null && resource instanceof DeltaVResource) {
            oR = ((DeltaVResource) resource).getOptionResponse(oInfo);
        }
        if (oR == null) {
            response.setStatus(DavServletResponse.SC_OK);
        } else {
            response.sendXmlResponse(oR, DavServletResponse.SC_OK);
        }
*/
        response.setStatus(DavServletResponse.SC_OK);
    }
    protected void doReport(WebdavRequest request, WebdavResponse response,
                            DavResource resource)
            throws DavException, IOException {
        ReportInfo info = request.getReportInfo();
        Report report;
        if (resource instanceof AddressbookResource) {
            report = ((AddressbookResource) resource).getReport(info);
        } else {
            response.sendError(DavServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        }

        int statusCode = (report.isMultiStatusReport()) ? DavServletResponse.SC_MULTI_STATUS : DavServletResponse.SC_OK;
        response.sendXmlResponse(report, statusCode);
    }



    private static class TheFileLogger implements FileLogger {
        private final File loggingDir;

        protected void serialize(Document doc, File file){
            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
                tr.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(file)));
            } catch (TransformerException | IOException ex) {
                LOGGER.debug(String.format("Failed to write file %s", file.getAbsolutePath()), ex);
            }
        }
        @Override
        public void log(Document doc) {
            if (loggingDir != null && loggingDir.exists()){
                serialize(doc, new File(loggingDir, String.format("%s_in.xml", System.currentTimeMillis())));
            }
        }

        @Override
        public void log(byte[] buffer) {
            if (loggingDir != null && loggingDir.exists()){
                try (FileOutputStream out = new FileOutputStream(new File(loggingDir, String.format("%s_out.xml", System.currentTimeMillis())))){
                    out.write(buffer);
                } catch(IOException ex){
                    LOGGER.debug("Failed to write response message document", ex);
                }
            }
        }

        public TheFileLogger(File loggingDir){
            this.loggingDir = loggingDir;
        }
    }
}