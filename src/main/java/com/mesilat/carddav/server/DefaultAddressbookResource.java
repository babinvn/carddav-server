package com.mesilat.carddav.server;

import java.util.ArrayList;
import java.util.List;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceLocator;

public class DefaultAddressbookResource extends AddressbookResource {
    private final String userKey;    
    private final List<VCardResource> vcards = new ArrayList<>();

    @Override
    public long getCTag(){
        return DatabaseService.getInstance().getCTag();
    }
    @Override
    public String getSyncToken(){
        return String.format("http://mesilat.com/ns/sync/%d", getCTag());
    }    

    @Override
    public DavResourceIterator getMembers() {
        if (vcards.isEmpty()) {
            DavLocatorFactory factory = getLocator().getFactory();
            int i = 0;
            for (VCardTag tag : DatabaseService.getInstance().list()){
                String location = String.format("/plugins/servlet/carddav/addressbooks/%s/default/%s.vcf", userKey, tag.getId());
                VCardResource vcard = new VCardResource(factory.createResourceLocator(getLocator().getPrefix(), location), tag.getId());
                vcards.add(vcard);
            }
        }
        return new VCardIterator(vcards);
    }

    public DefaultAddressbookResource(DavResourceLocator locator, String userKey){
        super(locator, userKey, "Default Address Book");
        this.userKey = userKey;
    }
}