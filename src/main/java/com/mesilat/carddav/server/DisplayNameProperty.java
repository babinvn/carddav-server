package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.property.AbstractDavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyName;

public class DisplayNameProperty extends AbstractDavProperty<String> {
    public static final DavPropertyName NAME = DavPropertyName.create(
            "displayname",
            DavConstants.NAMESPACE
    );

    private final String displayName;

    @Override
    public String getValue() {
        return displayName;
    }

    public DisplayNameProperty(String displayName){
        super(NAME, true);
        this.displayName = displayName;
    }
}