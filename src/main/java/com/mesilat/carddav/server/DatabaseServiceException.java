package com.mesilat.carddav.server;

public class DatabaseServiceException extends Exception {
    public DatabaseServiceException(String msg){
        super(msg);
    }
    public DatabaseServiceException(Throwable cause){
        super(cause);
    }
    public DatabaseServiceException(String msg, Throwable cause){
        super(msg, cause);
    }
}