package com.mesilat.carddav.server;

import java.io.IOException;
import net.sourceforge.cardme.io.VCardWriter;
import net.sourceforge.cardme.vcard.VCard;
import net.sourceforge.cardme.vcard.exceptions.VCardBuildException;
import net.sourceforge.cardme.vcard.exceptions.VCardParseException;
import org.apache.jackrabbit.webdav.property.AbstractDavProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class VCardProperty extends AbstractDavProperty<String> implements Constants {
    private static final Logger LOGGER = LoggerFactory.getLogger(LOG_NAME);
    private final String id;

    @Override
    public String getValue() {
        try {
            VCard vcard = DatabaseService.getInstance().getVCard(id);
            if (vcard == null){
                return null;
            }
            VCardWriter writer = new VCardWriter();
            writer.setVCard(vcard);
            return writer.buildVCardString();
        } catch (VCardBuildException | IOException | VCardParseException ex) {
            LOGGER.warn("net.sourceforge.cardme.io.VCardWriter.buildVCardString() failed", ex);
            return null;
        }
    }
    @Override
    public Element toXml(Document document) {
        Element elt = getName().toXml(document);
        elt.appendChild(document.createTextNode(getValue()));
        return elt;
    }

    public VCardProperty(String id){
        super(CardDavConstants.PROPERTY_NAME_ADDRESS_DATA, true);
        this.id = id;
    }
}