package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.AbstractLocatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DavLocatorFactoryImpl extends AbstractLocatorFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger("com.mesilat.crm-blueprints");

    public DavLocatorFactoryImpl(String pathPrefix) {
        super(pathPrefix);
    }

    @Override
    protected String getRepositoryPath(String resourcePath, String wspPath) {
        if (resourcePath == null) {
            return null;
        }
        if (resourcePath.equals(wspPath)) {
            // workspace
            LOGGER.debug("Resource path represents workspace path -> repository path is null.");
            return null;
        } else {
            // a repository item  -> remove wspPath + /jcr:root
            String pfx = wspPath + ItemResourceConstants.ROOT_ITEM_RESOURCEPATH;
            if (resourcePath.startsWith(pfx)) {
                String repositoryPath = resourcePath.substring(pfx.length());
                return (repositoryPath.length() == 0) ? ItemResourceConstants.ROOT_ITEM_PATH : repositoryPath;
            } else {
                LOGGER.error("Unexpected format of resource path.");
                throw new IllegalArgumentException("Unexpected format of resource path: " + resourcePath + " (workspace: " + wspPath + ")");
            }
        }
    }

    @Override
    protected String getResourcePath(String repositoryPath, String wspPath) {
        if (wspPath != null) {
            StringBuffer b = new StringBuffer(wspPath);
            if (repositoryPath != null) {
                b.append(ItemResourceConstants.ROOT_ITEM_RESOURCEPATH);
                if (!ItemResourceConstants.ROOT_ITEM_PATH.equals(repositoryPath)) {
                    b.append(repositoryPath);
                }
            }
            return b.toString();
        } else {
            LOGGER.debug("Workspace path is 'null' -> 'null' resource path");
            return null;
        }
    }
}