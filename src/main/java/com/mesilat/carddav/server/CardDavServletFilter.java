package com.mesilat.carddav.server;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class CardDavServletFilter implements Filter {
    private static final String AUTH_CHALLENGE = "WWW-Authenticate";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        if (req.getHeader("Authorization") == null){
            resp.addHeader(AUTH_CHALLENGE, "Basic realm=\"Confluence\"");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            chain.doFilter(request, new MyResponseWrapper((HttpServletResponse)response));
        }
    }
    @Override
    public void init(FilterConfig filterConfig) {        
    }
    @Override
    public void destroy() {        
    }
    
    private static class MyResponseWrapper extends HttpServletResponseWrapper {

        @Override
        public void addHeader(String name, String value) {
            if (AUTH_CHALLENGE.equals(name)){
                super.addHeader(AUTH_CHALLENGE, "Basic realm=\"Confluence\""); // Not OAuth, not for CardDAV
            } else {
                super.addHeader(name, value);
            }
        }

        public MyResponseWrapper(HttpServletResponse response) {
            super(response);
        }
    }
}