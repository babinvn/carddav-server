package com.mesilat.carddav.server;

import javax.xml.parsers.ParserConfigurationException;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.HrefProperty;
import org.apache.jackrabbit.webdav.property.ResourceType;
import org.apache.jackrabbit.webdav.security.CurrentUserPrivilegeSetProperty;
import org.apache.jackrabbit.webdav.security.Privilege;
import org.apache.jackrabbit.webdav.security.SecurityConstants;
import org.apache.jackrabbit.webdav.security.report.PrincipalSearchReport;
import org.apache.jackrabbit.webdav.security.report.SearchablePropertyReport;
import org.apache.jackrabbit.webdav.version.report.Report;
import org.apache.jackrabbit.webdav.version.report.ReportInfo;
import org.apache.jackrabbit.webdav.version.report.ReportType;
import org.apache.jackrabbit.webdav.version.report.SupportedReportSetProperty;
import org.apache.jackrabbit.webdav.xml.DomUtil;
import org.w3c.dom.Element;

public abstract class AddressbookResource extends DavResourceImpl {
    private static final int RESOURCE_TYPE_ADDRESSBOOK;
    private final String userKey;
    private final String displayName;
    private final DavLocatorFactory factory;
    private final String prefix;

    @Override
    public String getComplianceClass() {
        return "1, 3, access-control, addressbook";
    }
    @Override
    public String getSupportedMethods() {
        return "PROPFIND";
    }
    @Override
    public final String getDisplayName(){
        return displayName;
    }

    protected DavLocatorFactory getLocatorFactory(){
        return factory;
    }
    protected String getLocatorPrefix(){
        return prefix;
    }
    protected String getUserKey(){
        return userKey;
    }
    
    public Report getReport(ReportInfo reportInfo) throws DavException {
        if (reportInfo == null) {
            throw new DavException(DavServletResponse.SC_BAD_REQUEST, "A REPORT request must provide a valid XML request body.");
        }
        if (!exists()) {
            throw new DavException(DavServletResponse.SC_NOT_FOUND);
        }
        ReportType reportType = ReportType.getType(reportInfo);
        if (reportType != null && reportType.equals(AddressbookMultigetReport.TYPE)){
            AddressbookMultigetReport report = new AddressbookMultigetReport();
            report.init(this, reportInfo);
            return report;
        } else if (reportType != null && reportType.equals(SyncCollectionReport.TYPE)){
            SyncCollectionReport report = new SyncCollectionReport();
            report.init(this, reportInfo);
            return report;
        } else {
            Element condition = null;
            try {
                condition = DomUtil.createDocument().createElementNS("DAV:", "supported-report");
            } catch (ParserConfigurationException ex) {
                // we don't care THAT much
            }
            throw new DavException(DavServletResponse.SC_CONFLICT,
                "Unknown report '" + reportInfo.getReportName() + "' requested.", null, condition);
        }
    }
    public abstract long getCTag();
    public abstract String getSyncToken();

    public AddressbookResource(DavResourceLocator locator, String userKey, String displayName){
        super(locator, true);
        this.userKey = userKey;
        this.displayName = displayName;
        this.factory = locator.getFactory();
        this.prefix = locator.getPrefix();

        DavPropertySet props = getProperties();
        props.add(new CurrentUserPrivilegeSetProperty(new Privilege[]{
            Privilege.PRIVILEGE_READ, Privilege.PRIVILEGE_READ_ACL, Privilege.PRIVILEGE_READ_CURRENT_USER_PRIVILEGE_SET
        }));
        props.add(new HrefProperty(SecurityConstants.OWNER, new String[]{
            String.format("%s%s/servlet/carddav/principals/%s/", locator.getPrefix(), locator.getWorkspacePath(), userKey)
        }, false));
        props.add(new ResourceType(new int[] { ResourceType.COLLECTION, RESOURCE_TYPE_ADDRESSBOOK }));
        props.add(new SupportedReportSetProperty(new ReportType[]{
            ReportType.EXPAND_PROPERTY,
            PrincipalSearchReport.REPORT_TYPE, // principal-property-search
            SearchablePropertyReport.REPORT_TYPE, // principal-search-property-set
            SyncCollectionReport.TYPE,
            AddressbookMultigetReport.TYPE,
            AddressbookQueryReport.TYPE
        }));
        props.add(new DisplayNameProperty(getDisplayName()));
        props.add(new MaxResourceSizeProperty(10000000l));
        props.add(new SyncTokenProperty(getSyncToken()));
        props.add(new GetCTagProperty(getCTag()));
    }
    
    static {
        RESOURCE_TYPE_ADDRESSBOOK = ResourceType.registerResourceType(CardDavConstants.RESOURCE_TYPE_ADDRESSBOOK, CardDavConstants.CARDDAV_NAMESPACE);
    }
}