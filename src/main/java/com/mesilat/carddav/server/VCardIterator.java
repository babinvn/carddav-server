package com.mesilat.carddav.server;

import java.util.Iterator;
import java.util.List;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceIterator;

public class VCardIterator implements DavResourceIterator {
    private final Iterator<VCardResource> it;
    private final int size;

    @Override
    public DavResource nextResource() {
        return next();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean hasNext() {
        return it.hasNext();
    }

    @Override
    public DavResource next() {
        return it.next();
    }

    public VCardIterator(List<VCardResource> resources) {
        this.it = resources.iterator();
        this.size = resources.size();
    }
}