package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.version.DeltaVConstants;
import org.apache.jackrabbit.webdav.version.VersionControlledResource;
import org.apache.jackrabbit.webdav.version.VersionResource;
import org.apache.jackrabbit.webdav.version.report.Report;
import org.apache.jackrabbit.webdav.version.report.ReportInfo;
import org.apache.jackrabbit.webdav.version.report.ReportType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AddressbookQueryReport implements Report, DeltaVConstants {
    private static Logger log = LoggerFactory.getLogger(AddressbookQueryReport.class);

    public static final ReportType TYPE = ReportType.register("addressbook-query", CardDavConstants.CARDDAV_NAMESPACE, AddressbookQueryReport.class);
    private ReportInfo info;
    private DavResource resource;

    @Override
    public ReportType getType() {
        return TYPE;
    }

    @Override
    public boolean isMultiStatusReport() {
        return true;
    }

    @Override
    public void init(DavResource resource, ReportInfo info) throws DavException {
        setResource(resource);
        setInfo(info);
    }

    private void setResource(DavResource resource) throws DavException {
        // TODO
        if (resource != null && (resource instanceof VersionControlledResource || resource instanceof VersionResource)) {
            this.resource = resource;
        } else {
            throw new DavException(DavServletResponse.SC_BAD_REQUEST, "DAV:version-tree report can only be created for version-controlled resources and version resources.");
        }
    }

    private void setInfo(ReportInfo info) throws DavException {
        // TODO
        if (!getType().isRequestedReportType(info)) {
            throw new DavException(DavServletResponse.SC_BAD_REQUEST, "DAV:version-tree element expected.");
        }
        this.info = info;
    }

    @Override
    public Element toXml(Document document) {
        return getMultiStatus().toXml(document);
    }

    private MultiStatus getMultiStatus() {
        // TODO
        if (info == null || resource == null) {
            throw new NullPointerException("Error while running DAV:version-tree report");
        }

        MultiStatus ms = new MultiStatus();
        //buildResponse(resource, info.getPropertyNameSet(), info.getDepth(), ms);
        return ms;
    }
}