package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.property.AbstractDavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyName;

public class GetCTagProperty extends AbstractDavProperty<String> {
    public static final DavPropertyName NAME = DavPropertyName.create(
            "getctag",
            CalDavConstants.CALENDAR_SERVER_NAMESPACE
    );

    private final long ctag;

    @Override
    public String getValue() {
        return String.format("%d", ctag);
    }

    public GetCTagProperty(long ctag){
        super(NAME, true);
        this.ctag = ctag;
    }
}