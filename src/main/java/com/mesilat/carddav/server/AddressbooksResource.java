package com.mesilat.carddav.server;

import java.util.ArrayList;
import java.util.List;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.HrefProperty;
import org.apache.jackrabbit.webdav.property.ResourceType;
import org.apache.jackrabbit.webdav.security.CurrentUserPrivilegeSetProperty;
import org.apache.jackrabbit.webdav.security.Privilege;
import org.apache.jackrabbit.webdav.security.SecurityConstants;
import org.apache.jackrabbit.webdav.security.report.PrincipalSearchReport;
import org.apache.jackrabbit.webdav.security.report.SearchablePropertyReport;
import org.apache.jackrabbit.webdav.version.report.ReportType;
import org.apache.jackrabbit.webdav.version.report.SupportedReportSetProperty;

public class AddressbooksResource extends DavResourceImpl {
    private final List<AddressbookResource> addressBooks;
    private final String userDisplayName;

    @Override
    public String getComplianceClass() {
        return "1, 3, access-control, addressbook";
    }
    @Override
    public String getSupportedMethods() {
        return "PROPFIND";
    }
    @Override
    public final String getDisplayName(){
        return userDisplayName;
    }
    @Override
    public DavResourceIterator getMembers() {
        return new AddressbookIterator(addressBooks);
    }
    public long getCTag(){
        return DatabaseService.getInstance().getCTag();
    }
    public String getSyncToken(){
        return String.format("http://mesilat.com/ns/sync/%d", getCTag());
    }    

    public AddressbooksResource(DavResourceLocator locator, String userKey, String userDisplayName){
        super(locator, true);
        this.userDisplayName = userDisplayName;

        DavLocatorFactory factory = locator.getFactory();
        this.addressBooks = new ArrayList<>();
        String defaultAddressBook = String.format("%s/servlet/carddav/addressbooks/%s/default/", locator.getWorkspacePath(), userKey);
        this.addressBooks.add(new DefaultAddressbookResource(factory.createResourceLocator(locator.getPrefix(), defaultAddressBook), userKey));

        DavPropertySet props = getProperties();
        props.add(new CurrentUserPrivilegeSetProperty(new Privilege[]{
            Privilege.PRIVILEGE_READ, Privilege.PRIVILEGE_READ_ACL, Privilege.PRIVILEGE_READ_CURRENT_USER_PRIVILEGE_SET
        }));
        props.add(new HrefProperty(SecurityConstants.OWNER, new String[]{
            String.format("%s%s/servlet/carddav/principals/%s/", locator.getPrefix(), locator.getWorkspacePath(), userKey)
        }, false));
        props.add(new ResourceType(ResourceType.COLLECTION));
        props.add(new SupportedReportSetProperty(new ReportType[]{
            ReportType.EXPAND_PROPERTY,
            PrincipalSearchReport.REPORT_TYPE, // principal-property-search
            SearchablePropertyReport.REPORT_TYPE // principal-search-property-set
        }));
        props.add(new SyncTokenProperty(getSyncToken()));
    }
}