package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.property.AbstractDavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyName;

public class SyncTokenProperty extends AbstractDavProperty<String> {
    public static final DavPropertyName NAME = DavPropertyName.create(
            "sync-token",
            DavConstants.NAMESPACE
    );

    private final String syncToken;

    @Override
    public String getValue() {
        return syncToken;
    }

    public SyncTokenProperty(String syncToken){
        super(NAME, true);
        this.syncToken = syncToken;
    }
}