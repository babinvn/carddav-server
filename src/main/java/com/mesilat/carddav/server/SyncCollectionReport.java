package com.mesilat.carddav.server;

import javax.servlet.http.HttpServletResponse;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;
import org.apache.jackrabbit.webdav.version.DeltaVConstants;
import org.apache.jackrabbit.webdav.version.report.Report;
import org.apache.jackrabbit.webdav.version.report.ReportInfo;
import org.apache.jackrabbit.webdav.version.report.ReportType;
import org.apache.jackrabbit.webdav.xml.DomUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SyncCollectionReport implements Report, DeltaVConstants, Constants {
    public static final ReportType TYPE = ReportType.register("sync-collection", DavConstants.NAMESPACE, SyncCollectionReport.class);

    private ReportInfo info;
    private DavResource resource;
    private final String userKey;

    @Override
    public ReportType getType() {
        return TYPE;
    }

    @Override
    public boolean isMultiStatusReport() {
        return true;
    }

    @Override
    public void init(DavResource resource, ReportInfo info) throws DavException {
        setResource(resource);
        setInfo(info);
    }

    private void setResource(DavResource resource) throws DavException {
        this.resource = resource;
    }

    private void setInfo(ReportInfo info) throws DavException {
        // TODO
        if (!getType().isRequestedReportType(info)) {
            throw new DavException(DavServletResponse.SC_BAD_REQUEST, "DAV:version-tree element expected.");
        }
        this.info = info;
    }

    @Override
    public Element toXml(Document document) {
        return getMultiStatus(userKey).toXml(document);
    }

    private MultiStatus getMultiStatus(String userKey) {
        if (info == null || resource == null) {
            throw new NullPointerException("Error while running DAV:version-tree report");
        }

        MultiStatus ms = new SyncCollectionMultistatus();
        buildResponse(resource, info.getPropertyNameSet(), ms, userKey);
        return ms;
    }
    private String getSyncToken(){
        Element e = info.getContentElement("sync-token", NAMESPACE);
        return e.getTextContent().trim();
    }
    private Long getSyncTokenTime(){
        Element e = info.getContentElement("sync-token", NAMESPACE);
        try {
            return Long.parseLong(getSyncToken().substring(27));
        } catch(Throwable ignore){
            return 0L;
        }
    }
    private long getCTag(){
        return DatabaseService.getInstance().getCTag();
    }
    private void buildResponse(DavResource res, DavPropertyNameSet propNameSet, MultiStatus ms, String userKey){
        if (res instanceof AddressbookResource){
            AddressbookResource addressBook = (AddressbookResource)res;
            for (VCardTag tag: DatabaseService.getInstance().list(getSyncTokenTime())){
                String location = String.format("/plugins/servlet/carddav/addressbooks/%s/default/%s.vcf", userKey, tag.getId());
                VCardResource vcard = new VCardResource(addressBook.getLocatorFactory().createResourceLocator(addressBook.getLocatorPrefix(), location), tag.getId());
                if ("A".equals(tag.getStatus())){
                    ms.addResourceProperties(vcard, propNameSet, 0);
                } else {
                    ms.addResourceStatus(vcard, HttpServletResponse.SC_NOT_FOUND, 0); // 
                }
            }
        } else {
            throw new IllegalArgumentException("Addressbook resource was expected");
        }
    }
    public SyncCollectionReport(){
        this.userKey = DatabaseService.getInstance().getUserKey();
    }

    public class SyncCollectionMultistatus extends MultiStatus {
        @Override
        public Element toXml(Document document) {
            Element multistatus = DomUtil.createElement(document, XML_MULTISTATUS, NAMESPACE);
            for (MultiStatusResponse resp : getResponses()) {
                multistatus.appendChild(resp.toXml(document));
            }
            Element syncToken = DomUtil.createElement(document, "sync-token", NAMESPACE);
            //syncToken.setTextContent(getSyncToken());
            syncToken.setTextContent(String.format("http://mesilat.com/ns/sync/%d", getCTag()));
            multistatus.appendChild(syncToken);
            return multistatus;
        }
    }
}