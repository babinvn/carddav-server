package com.mesilat.carddav.server;

public interface VCardTag {
    String getId();
    void setId(String id);
    long getETag();
    void setETag(long etag);
    String getStatus();
    void setStatus(String status);
}