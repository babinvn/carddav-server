package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.DefaultDavProperty;

public class VCardResource extends DavResourceImpl {
    private final Long etag;

    @Override
    public String getComplianceClass() {
        return "1, 3, access-control, addressbook";
    }
    @Override
    public String getSupportedMethods() {
        return "PROPFIND";
    }
    @Override
    public boolean isCollection() {
        return false;
    }
    public Long getETag(){
        return etag;
    }

    public VCardResource(DavResourceLocator locator, String id){
        super(locator, true);
        this.etag = DatabaseService.getInstance().getETag(id);

        DavPropertySet props = getProperties();
        props.add(new DefaultDavProperty<>(DavPropertyName.GETETAG, String.format("\"%d\"", this.etag)));
        props.add(new VCardProperty(id));
    }
}