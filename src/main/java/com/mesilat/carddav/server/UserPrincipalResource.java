package com.mesilat.carddav.server;

import java.util.ArrayList;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.HrefProperty;
import org.apache.jackrabbit.webdav.security.report.PrincipalSearchReport;
import org.apache.jackrabbit.webdav.security.report.SearchablePropertyReport;
import org.apache.jackrabbit.webdav.version.report.ReportType;
import org.apache.jackrabbit.webdav.version.report.SupportedReportSetProperty;

public class UserPrincipalResource extends DavResourceImpl {
    private final String userDisplayName;

    @Override
    public String getComplianceClass() {
        return "1, 3, access-control, addressbook";
    }
    @Override
    public String getSupportedMethods() {
        return "OPTIONS, GET, HEAD, PROPFIND, REPORT";
    }

    @Override
    public final String getDisplayName(){
        return userDisplayName;
    }
    @Override
    public DavResourceIterator getMembers() {
        return new MyIterator(new ArrayList<>());
    }

    public UserPrincipalResource(DavResourceLocator locator, String userKey, String userDisplayName){
        super(locator, true);
        this.userDisplayName = userDisplayName;

        DavPropertySet props = getProperties();
        props.add(new HrefProperty(CardDavConstants.PROPERTY_NAME_ADDRESSBOOK_HOME_SET, new String[]{
            String.format("%s%s/servlet/carddav/addressbooks/%s/", locator.getPrefix(), locator.getWorkspacePath(), userKey)
        }, false));
        props.add(new DisplayNameProperty(getDisplayName()));
        props.add(new HrefProperty(DavPropertyName.create("principal-collection-set", DavConstants.NAMESPACE), new String[]{
            String.format("%s%s/servlet/carddav/addressbooks/", locator.getPrefix(), locator.getWorkspacePath())
        }, false));
        props.add(new HrefProperty(DavPropertyName.create("principal-URL", DavConstants.NAMESPACE), new String[]{
            String.format("%s%s/servlet/carddav/principals/%s/", locator.getPrefix(), locator.getWorkspacePath(), userKey)
        }, true));
        props.add(new SupportedReportSetProperty(new ReportType[]{
            ReportType.EXPAND_PROPERTY,
            PrincipalSearchReport.REPORT_TYPE, // principal-property-search
            SearchablePropertyReport.REPORT_TYPE // principal-search-property-set
        }));
    }
}