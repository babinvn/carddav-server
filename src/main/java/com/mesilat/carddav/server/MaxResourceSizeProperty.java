package com.mesilat.carddav.server;

import org.apache.jackrabbit.webdav.property.AbstractDavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyName;

public class MaxResourceSizeProperty extends AbstractDavProperty<Long> {
    public static final DavPropertyName NAME = DavPropertyName.create(
            "max-resource-size",
            CardDavConstants.CARDDAV_NAMESPACE
    );

    private final Long value;

    @Override
    public Long getValue() {
        return value;
    }

    public MaxResourceSizeProperty(Long value){
        super(NAME, true);
        this.value = value;
    }
}