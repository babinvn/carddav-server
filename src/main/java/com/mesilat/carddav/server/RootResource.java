package com.mesilat.carddav.server;

import java.util.ArrayList;
import java.util.List;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.HrefProperty;
import org.apache.jackrabbit.webdav.property.ResourceType;

public class RootResource extends DavResourceImpl {
    private final String userKey;
    private final String userDisplayName;

    @Override
    public String getComplianceClass() {
        return "1, 3, access-control, addressbook";
    }
    @Override
    public String getSupportedMethods() {
        return "OPTIONS, GET, HEAD, PROPFIND, REPORT";
    }
    @Override
    public DavResourceIterator getMembers() {
        DavLocatorFactory factory = getLocator().getFactory();
        List<DavResource> resources = new ArrayList<>();
        String path = String.format("%s%s/principals/%s/", getLocator().getWorkspacePath(), getLocator().getRepositoryPath(), userKey);
        resources.add(new UserPrincipalResource(factory.createResourceLocator(getLocator().getPrefix(), path), userKey, userDisplayName));
        path = String.format("%s%s/addressbooks/%s/", getLocator().getWorkspacePath(), getLocator().getRepositoryPath(), userKey);
        resources.add(new AddressbooksResource(factory.createResourceLocator(getLocator().getPrefix(), path), userKey, userDisplayName));
        return new MyIterator(resources);
    }
    public RootResource(DavResourceLocator locator, String userKey, String userDisplayName){
        super(locator, true);

        this.userKey = userKey;
        this.userDisplayName = userDisplayName;

        DavPropertySet props = getProperties();
        props.add(new ResourceType(ResourceType.COLLECTION));
        props.add(new HrefProperty(DavPropertyName.create("current-user-principal", DavConstants.NAMESPACE), new String[]{
            String.format("%s%s%s/principals/%s/", locator.getPrefix(), locator.getWorkspacePath(), locator.getRepositoryPath(), userKey)
        }, true));
        props.add(new HrefProperty(DavPropertyName.create("principal-URL", DavConstants.NAMESPACE), new String[]{
            String.format("%s%s%s/principals/%s/", locator.getPrefix(), locator.getWorkspacePath(), locator.getRepositoryPath(), userKey)
        }, true));
    }
}