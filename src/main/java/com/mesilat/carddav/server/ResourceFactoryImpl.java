package com.mesilat.carddav.server;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletResponse;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.DavServletRequest;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.apache.jackrabbit.webdav.DavSession;

public class ResourceFactoryImpl implements DavResourceFactory {
    private static final String ROOT = "/plugins/servlet/carddav";
    private static final Pattern PRINCIPALS = Pattern.compile("/plugins/servlet/carddav/principals/(.+)");
    private static final Pattern ADDRESSBOOKS = Pattern.compile("/plugins/servlet/carddav/addressbooks/(.+)");
    private static final Pattern DEFAULT_ADDRESSBOOK = Pattern.compile("/plugins/servlet/carddav/addressbooks/(.+)/default");

    private final String userKey;
    private final String userDisplayName;

    public ResourceFactoryImpl(String userKey, String userDisplayName) {
        this.userKey = userKey;
        this.userDisplayName = userDisplayName;
    }

    @Override
    public DavResource createResource(DavResourceLocator locator, DavServletRequest request, DavServletResponse response) throws DavException {
        if (ROOT.equals(locator.getResourcePath())){
            return new RootResource(locator, userKey, userDisplayName);
        }

        Matcher m = PRINCIPALS.matcher(locator.getResourcePath());
        if (m.matches()){
            String principal = m.group(1);
            if (!userKey.equals(principal)){
                throw new DavException(HttpServletResponse.SC_FORBIDDEN);
            }
            return new UserPrincipalResource(locator, userKey, userDisplayName);
        }

        m = DEFAULT_ADDRESSBOOK.matcher(locator.getResourcePath());
        if (m.matches()){
            String principal = m.group(1);
            if (!userKey.equals(principal)){
                throw new DavException(HttpServletResponse.SC_FORBIDDEN);
            }
            return new DefaultAddressbookResource(locator, userKey);
        }

        m = ADDRESSBOOKS.matcher(locator.getResourcePath());
        if (m.matches()){
            String principal = m.group(1);
            if (!userKey.equals(principal)){
                throw new DavException(HttpServletResponse.SC_FORBIDDEN);
            }
            return new AddressbooksResource(locator, userKey, userDisplayName);
        }

        return new DavResourceImpl(locator, true);
    }
    @Override
    public DavResource createResource(DavResourceLocator locator, DavSession session) throws DavException {
        return new DavResourceImpl(locator, true);
    }
}