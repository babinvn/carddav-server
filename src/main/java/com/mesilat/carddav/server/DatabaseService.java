package com.mesilat.carddav.server;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import net.sourceforge.cardme.vcard.VCard;
import net.sourceforge.cardme.vcard.exceptions.VCardParseException;

public interface DatabaseService {
    long getETag(String id);
    long getCTag();
    VCard getVCard(String id) throws IOException, VCardParseException;
    List<VCardTag> list();
    List<VCardTag> list(long ctag);
    String getUserKey();
    String getUserDisplayName();

    static final AtomicReference<DatabaseService> INSTANCE = new AtomicReference<>();
    public static DatabaseService getInstance(){
        return INSTANCE.get();
    }
    static void setInstance(DatabaseService instance){
        DatabaseService.INSTANCE.set(instance);
    }
}