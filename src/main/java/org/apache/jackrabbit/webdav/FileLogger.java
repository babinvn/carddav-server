package org.apache.jackrabbit.webdav;

import org.w3c.dom.Document;

public interface FileLogger {
    void log(Document doc);
    void log(byte[] buffer);
}